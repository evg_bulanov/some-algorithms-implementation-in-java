package some.inverview.questions.spr;

/**
 * super keyword:
 * - If a method overrides one of its superclass's methods, the overridden can be invoked through the use of the keyword super.
 * - The super keyword can also be used to refer to a hidden field of the super type.
 * - Interface default methods can be invoked using the super keyword.
 * - Constructors for superclasses can be invoked using the super keyword.
 */
public class ChildClass extends ParentClass implements AnInterface {
    int counter = 0;

    public ChildClass() {
        // invoking parent class's constructor
        // using the super keyword
        super(9);
    }

    public void accessHiddenFields() {
        // accessing parent class's counter field which gets
        // hidden with the local counter field.
        super.counter++;
        counter++;
        System.out.println("sublcass counter: " + counter + " parent class counter: " + super.counter);

        // accessing the interface's default method which is hidden
        AnInterface.super.accessHiddenFields();
    }

    public static void main(String[] args) {
        (new ChildClass()).accessHiddenFields();
    }
}

interface AnInterface {
    default void accessHiddenFields() {
        System.out.println("Default method of AnInterface invoked.");
    }
}

class ParentClass {
    protected int counter;

    public ParentClass(int val) {
        this.counter = val;
    }
}