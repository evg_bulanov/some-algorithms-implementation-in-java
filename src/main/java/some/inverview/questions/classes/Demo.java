package some.inverview.questions.classes;

public class Demo {
    public static void main( String args[] ) {
        Demo[] array = new Demo[1];
        array[0] = new Demo();

        if( array[0] instanceof Object) {
            System.out.println( "Demonstration is instance of Object");
        }

        if( array instanceof Object) {
            System.out.println( "Array is instance of Object");
        }
    }
}
