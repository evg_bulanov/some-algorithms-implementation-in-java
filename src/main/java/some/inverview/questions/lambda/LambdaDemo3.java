package some.inverview.questions.lambda;

/**
 * Lambda can return value
 */
public class LambdaDemo3 {
    public static void main( String args[] ) {
        int anyNumber = 5;
        RaiseToPower square = (x) -> Math.pow(x, 2);
        RaiseToPower cube = (x) -> Math.pow(x, 3);

        System.out.println(square.raiseToX(5));
        System.out.println(cube.raiseToX(5));
    }

    interface RaiseToPower {
        double raiseToX(int x);
    }
}
