package some.inverview.questions.lambda;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Lambda can have 0, 1, 2 or more params.
 */
public class LambdaDemo2 {
    public static void main( String args[] ) {
        Comparator<Integer> descendingComparator = (i1, i2) -> i2 - i1;
        PriorityQueue<Integer> q = new PriorityQueue<>(descendingComparator);

        // add integers to q in ascending order
        for (int i = 0; i <= 10; i++) {
            q.add(i);
        }

        // verify the numbers are printed in descending order
        for (int i = 0; i <= 10; i++) {
            System.out.println(q.poll());
        }

    }
}
