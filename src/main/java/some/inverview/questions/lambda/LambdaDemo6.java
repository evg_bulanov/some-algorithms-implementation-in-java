package some.inverview.questions.lambda;

import java.util.concurrent.Callable;


public class LambdaDemo6 {
    public static void main( String args[] ) throws Exception{
        (new LambdaTargetType()).getWorking();
    }
}

/**
 * Java compiler can determine a target type. Since the expression returns a string, the compiler matches the call with
 * the compute method that accepts a type of Callable and thus the lambda expression () -> "done" is of type callable.
 */
class LambdaTargetType {
    public void getWorking() throws Exception {
        compute(() -> "done");
    }

    void compute(Runnable r) {
        System.out.println("Runnable invoked");
        r.run();
    }

    <T> void compute(Callable<T> c) throws Exception {
        System.out.println("Callable invoked");
        c.call();
    }
}