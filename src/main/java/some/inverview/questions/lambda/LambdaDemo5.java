package some.inverview.questions.lambda;

/**
 * Lambda expressions can capture local, instance, and static variables.
 * Also note that unlike anonymous classes, lambda expressions don't suffer from shadowing.
 * They do not inherit any names from a supertype or introduce a new level of scoping.
 * Declarations in a lambda expression are interpreted just as they are in the enclosing environment.
 */

public class LambdaDemo5 {
    public static void main( String args[] ) {
        Work w = (new LambdaVariableCapture()).captureVariables();
        w.work();
    }
}

class LambdaVariableCapture {
    private static String staticVar = "static variable";
    private String instVar = "instance variable";

    public Work captureVariables() {

        int i = 0;
        Work w = () -> {
             // int i; //declaring i would result in compile error.

            String instVar = "lambda variable";
            System.out.println("i = " + i);
            System.out.println(staticVar);
            System.out.println(this.instVar);
            System.out.println(instVar);

            // Let's check what is this pointing to
            System.out.println(this.getClass());
        };

        staticVar = "changed static var";
        instVar = "changed instance var";

        return w;
    }
}

interface Work {

    void work();

}