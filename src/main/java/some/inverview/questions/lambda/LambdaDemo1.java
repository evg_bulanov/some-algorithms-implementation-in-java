package some.inverview.questions.lambda;

/**
 * Lambda expressions allow us to pass code as data or functionality as a method argument.
 *
 * Lambda expressions can be used to replace writing anonymous classes, when the class implements a functional interface.
 *
 * A functional interface has only a single abstract method and may have default or static methods.
 *
 *  with lambda you can just implement interfaces with one abstract method (other methods could be default).
 *
 * https://docs.oracle.com/javase/9/docs/api/java/util/function/BiFunction.html
 */
public class LambdaDemo1 {
    public static void main(String[] args) {
        DoStuff ds = new DoStuff() {
            @Override
            public void work() {
                System.out.println("I ran via anonymous class");
            }
        };
        ds.work();

        DoStuff ds1 = () -> System.out.println("I ran via lambda expression");
        ds1.work();
    }
}

interface DoStuff {
    void work();
}