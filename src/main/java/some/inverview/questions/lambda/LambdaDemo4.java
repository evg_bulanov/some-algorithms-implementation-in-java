package some.inverview.questions.lambda;

/**
 * can lambda be thought of as replacement of anonymous classes for implementing functional interfaces?
 *
 * No, one primary difference between the two is that the anonymous classes can maintain state as they can have instance or static variables
 *      whereas lambda expressions are just method implementations.
 *
 *
 *  The process of automatically deducing unspecified data types of an expression based on the contextual information
 *          is called type inference.
 *
 * Specifying parameter types for a lambda expression may sometimes be necessary if the compiler cannot infer
 * the parameter types from the functional interface method the lambda is matching.
 */
public class LambdaDemo4 {
    public static void main( String args[] ) {

        // Without specifying the data types of the method arguments
        CrunchNumbers cn1 = (k, j) -> {
            System.out.println(k % j == 0 ? "exactly divisible" : "not divisible");
        };

        // With specifying the data types of the method arguments
        CrunchNumbers cn2 = (int k, int j) -> {
            System.out.println(k % j == 0 ? "exactly divisible" : "not divisible");
        };

        cn1.work(4, 2);
        cn2.work(4, 2);
    }
}

interface CrunchNumbers {
    void work(int a, int b);
}
