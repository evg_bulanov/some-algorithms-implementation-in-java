package some.inverview.questions.fnlz;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;

/**
 * The finalize() method is a protected and non-static method of the java.lang.Object class
 * and is also sometimes called the finalizer.
 *
 * The finalize() method is invoked by the JVM when an object is garbage collected.
 *
 * A user can override the finalize() in his class to dispose of system resources
 * or to perform cleanup tasks.
 */
class MyWeirdInputStreamReaderClass {
    BufferedInputStream bis = null;

    void someReadInputMethod() throws FileNotFoundException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("pathToFile"));
        // ... logic to initialize bis variable but user
        // doesn't close the stream
    }

    /**
     * several reasons to not override the finalize() method:
     *  - not possible to predict when GC will invoke the finalize()
     *  - would not be even run before your Java program exits.
     *  - may not be invoked at all or in time to free-up resources and may cause a program crash.
     *  - GW depends on some program
     *  - performance may be negatively impacted
     *  - If it throws an exception, the finalization process is canceled, and the exception is ignored,
     *          leaving the object in a corrupted state without any notification.
     */
    // Don't use finalize in production code, bad idea!
    //    @Deprecated(since="9")
    @Override
    protected void finalize() {
        try {
            bis.close();
        } catch (IOException io) {
            // ... log a message
        }
    }

    /**
     * alternatives to finalizing objects:
     * - try-with-resources idiom (AutoCloseable interface)
     * - PhantomReference
     * - implement a close() method and document that the method be called
     */

    /**
     * Java invokes the finalizer on an object at most one time.
     * Even if the object is resurrected in the finalize method,
     * it'll not have finalize invoked on it the next time it becomes garbage eligible.
     */
}

class UsefulObjectClass {
    HashSet<UsefulObjectClass> immortals;

    public UsefulObjectClass(HashSet<UsefulObjectClass> immortals) {
        this.immortals = immortals;
    }

    @Override
    public void finalize() {
        System.out.println("I am being finalized.");
        immortals.add(this);
    }

    public void printName() {
        System.out.println("Hi, I am a useful object.");
    }
}

class Demonstration {
    static HashSet<UsefulObjectClass> immortals = new HashSet<>();

    public static void main( String args[] ) throws InterruptedException {
        UsefulObjectClass obj = new UsefulObjectClass(immortals);
        obj.printName();
        obj = null;

        Runtime.getRuntime().gc();

        // Sleep the main thread so that the garbage collector thread performs finalization
        Thread.sleep(1000);

        if (immortals.size() == 1) {
            System.out.println("Useful object saved from garbage collection.");
        }

        System.out.println("exiting");
    }
}