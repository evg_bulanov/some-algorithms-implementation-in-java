package some.inverview.questions.brdg;

/*
Client -------->  Abstraction (Restaurant)                    Pizza
                    - impl: Implementation                    <Interface>
                       + doThis1()            <>---------->   Implementation
                       + doThis2()                              + doThat1()
                            ^                                   + doThat2()
                            |                                   + doThat3()
                            |                                        ^
                    RefinedAbstraction                               |
                      AmericanRestaurant                             |
                      ItalianRestaurant                    ConcreteImplementation
                                                               PepperoniPizza
                                                               VeggiePizza
 */
public class BridgeDemo {
    static abstract class Pizza {
        String sauce;
        String toppings;
        String crust;

        abstract void assemble();
        abstract void qualityCheck();

        public void setSauce(String sauce) {
            this.sauce = sauce;
        }

        public void setToppings(String toppings) {
            this.toppings = toppings;
        }

        public void setCrust(String crust) {
            this.crust = crust;
        }
    }

    static class PepperoniPizza extends Pizza {
        @Override
        void assemble() {
            System.out.println("Adding Sauce: " + sauce);
            System.out.println("Adding Toppings: " + toppings);
            System.out.println("Adding Pepperoni");
        }

        @Override
        void qualityCheck() {
            System.out.println("Crust is : " + crust);
        }
    }

    static class VeggiePizza extends Pizza {
        @Override
        void assemble() {
            System.out.println("Adding Sauce: " + sauce);
            System.out.println("Adding Toppings: " + toppings);
            System.out.println("Adding Cheese");
        }

        @Override
        void qualityCheck() {
            System.out.println("Crust is : " + crust);
        }
    }

    // Bridge class for pizza
    static abstract class Restaurant {
        protected Pizza pizza;

        public Restaurant(Pizza pizza) {
            this.pizza = pizza;
        }

        abstract void addSauce();
        abstract void addToppings();
        abstract void makeCrust();

        public void deliver() {
            makeCrust();
            addSauce();
            addToppings();
            pizza.assemble();
            pizza.qualityCheck();
            System.out.println("Order in progress!!");
        }
    }

    static class ItalianRestaurant extends Restaurant {
        public ItalianRestaurant(Pizza pizza) {
            super(pizza);
        }

        @Override
        void addSauce() {
            pizza.setSauce("Oil");
        }

        @Override
        void addToppings() {
            pizza.setToppings(null);
        }

        @Override
        void makeCrust() {
            pizza.setCrust("Thin");
        }
    }

    static class AmericanRestaurant extends Restaurant {
        public AmericanRestaurant(Pizza pizza) {
            super(pizza);
        }

        @Override
        void addSauce() {
            pizza.setSauce("Super secret recipe");
        }

        @Override
        void addToppings() {
            pizza.setToppings("Everything");
        }

        @Override
        void makeCrust() {
            pizza.setCrust("Thick");
        }
    }

    public static void main(String[] args) {
        AmericanRestaurant americanRestaurant = new AmericanRestaurant(new PepperoniPizza());
        americanRestaurant.deliver();

        ItalianRestaurant italianRestaurant = new ItalianRestaurant(new VeggiePizza());
        italianRestaurant.deliver();
    }
}
