package some.inverview.questions.sngl;

public class SingletonDemo {
    /**
     * Example of not thead safe singleton
     */
    public static class SimpleSingleton {
        private static SimpleSingleton instance = null;

        private SimpleSingleton() {
        }

        public static SimpleSingleton getInstance() {
            if (instance == null) {
                instance = new SimpleSingleton();
            }
            return instance;
        }
    }

    /**
     * Example of thread safe singleton
     */
    public static class TheadSafeSingleton {
        private static volatile TheadSafeSingleton instance;
        private static Object mutex = new Object();

        private TheadSafeSingleton() {
        }

        public static TheadSafeSingleton getInstance() {
            TheadSafeSingleton result = instance;
            if (result == null) {
                synchronized (mutex) {
                    result = instance;
                    if (result == null)
                        instance = result = new TheadSafeSingleton();
                }
            }
            return result;
        }
    }

    /** Singleton as enum */
    public enum SingletonEnum {
        INSTANCE;

        int value;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    /**
     * In spring, java ee, cdi, guice, etc singleton is created by applying appropriate annotations:
     * @Singleton, @Component (by default it's singleton in spring), etc.
     */
    public static class SpringJavaEEGuiceSingleton {
    }


}
