package some.inverview.questions.prt;

import javax.swing.*;

public class PrototypeDemo {
    /** The idea of prototype object is simple, instead of creating object outside we have clone method. */
    public static abstract class Box {
        protected int height;
        protected int width;
        protected int depth;

        public abstract <T extends Box> T copy(Box box);
    }

    public static class Container extends Box {
        private final int height;
        private final int width;
        private final int depth;

        public Container() {
            this(0, 0, 0);
        }

        public Container(int height, int width, int depth) {
            this.height = height;
            this.width = width;
            this.depth = depth;
        }

        @Override
        public Container copy(Box box) {
            return new Container(box.height, box.width, box.depth);
        }
    }


}
