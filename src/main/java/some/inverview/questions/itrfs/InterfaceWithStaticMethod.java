package some.inverview.questions.itrfs;

class Demonstration2 implements InterfaceWithStaticMethod{
    public static void main( String args[] ) {
        printName();
        InterfaceWithStaticMethod.printName();
        Demonstration2.printName();
    }

    public static void printName() {
        System.out.println("Demonstration class");
    }
}


interface InterfaceWithStaticMethod {

    static void printName() {
        System.out.println("Interface with InterfaceWithStaticMethod");
    }

}