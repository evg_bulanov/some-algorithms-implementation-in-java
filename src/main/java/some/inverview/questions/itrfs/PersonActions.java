package some.inverview.questions.itrfs;

/**
 * An interface can be thought of as a contract to its consumers.
 * - the consumer is only knowledgeable about the interface and not the concrete classes that implement it.
 * - this makes the code more flexible and maintainable.
 *
 * Methods in an interface that are not declared as default or static are implicitly abstract.
 */
public interface PersonActions {
    void sayHello();

    // default method
    default void sayBye() {
        System.out.println("Say bye in english and override method for other languages.");
    }

    // static method
    static void printTotalPossibleActions() {
        System.out.println("Total possible acitons = 2");
    }
}

/**
 * difference between an interface and an abstract class:
 * - abstract class can have final, static, or class member variables
 *      whereas an interface can only have variables that are final and static by default.
 * - abstract class can have static, abstract, or non-abstract methods.
 *      an interface can have static, abstract, or default methods.
 * - abstract class can have varying visibility of private, protected, or public.
 *      in an interface all methods and constants are public.
 *  - abstract methods with the same or less restrictive accessibility,
 *      a class implementing an interface must define the interface methods (public)
 *  - an abstract class, a concrete child class must define the abstract methods
 *      an abstract class can extend another abstract class and abstract methods
 *      from the parent class don't have to be defined.
 *   -> an interface extending another interface is not responsible for implementing methods from the parent interface.
 *      This is because interfaces cannot define any implementation
 *  - A class can only extend another class, but it can implement multiple interfaces.
 *      Similarly, an interface can extend multiple interfaces.
 *      An interface never implements a class or an interface.
 *
 */
class Demonstration implements PersonActions {
    public static void main( String args[] ) {
        // Note the class Demonstration doesn't provide implementation
        // for the sayBye method and uses the default one.
        (new Demonstration()).sayBye();
    }

    @Override
    public void sayHello() {
        System.out.println("Say HI in english and override method for other languages.");
    }
}

/**
 * valid marker interface, ex: java.io.Serializable, java.lang.Cloneable, java.rmi.Remote
 */
interface IAmEmpty {
}

/**

for-each is possible because the List interface extends the Collection interface,
 which in turn extends the Iterable interface.

 void printName(List<String> names) {
    for(String name : names) {
        System.out.println(name);
    }
}

 The for-each is possible because the List interface extends the Collection interface which in turn extends the Iterable interface.
 Any class that implements the List interface will also be required to implement the Iterable interface.
 The Iterable interface allows iteration in the for-each loop. The above code is equivalent to the following snippet:

for (Iterator<String> it = names.iterator(); it.hasNext();) {
    String name = it.next();
    System.out.println(name);
}
 */

/**
 * functional interfaces in Java
 *  -  is an interface that contains only one abstract method.
 *
 *  - from Java 8 onwards, lambda expressions can be used to represent the instance of a functional interface
 *  - a functional interface can have any number of default methods.
 *
 * Ex: runnable interface
 */
interface Runnable {
    void run();
}

/**
 * Comparable vs Comparator
 * The Comparable interface is used to define a total and natural ordering on the objects of a type.
 * The Comparator is a functional interface that can be used to specify an ordering relation between
 *      the elements of a type other than their natural order.
 */

/**
 * Re-Abstracting Methods
 * If an abstract class or interface A declares a method as abstract for which a default implementation exists in some superinterface X,
 * the default implementation of X is overridden. Hence all concrete classes which subtype A must implement the method.
 * This can be used as an effective tool to enforce the reimplementation of inappropriate default implementations.
 *
 * This technique is used throughout the JDK, e.g. on ConcurrentMap (link) which
 *      re-abstracts a number of methods for which Map (link.
 *
 * Note that concrete classes can not explicitly call the overridden default implementation.
 */

/**
 * Overriding Methods on 'Object'
 * It is not possible for an interface to provide default implementations for the methods in Object.
 * Trying to do so will result in a compile error.
 */