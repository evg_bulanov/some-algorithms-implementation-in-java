package some.inverview.questions.itrfs;

public class Question {
    public static class Language {
        static String lang = "base language";

        static protected void printLanguage() {
            System.out.println(lang);
        }

        protected Language sayHello() {
            System.out.println("----");
            return this;
        }
    }

    public static class Spanish extends Language {
        static String lang = "Spanish";

        static protected void printLanguage() {
            System.out.println(lang);
        }

        protected Language sayHello() {
            System.out.println("Ola!");
            return this;
        }
    }

    public static void main(String[] args) {
        // question 1: output Ola!
        (new Spanish()).sayHello();

        // question 2: output Ola!
        Language lg = new Spanish();
        lg.sayHello();

        // question 3: remove printLanguage from Spanish
        // https://docs.oracle.com/javase/specs/jls/se8/html/jls-8.html#jls-8.4.8
        // output base language
        Spanish.printLanguage();
    }
}

class Question2 {
    public static interface Vehicle {
        default void whatAmI() {
            System.out.println("I am a vehicle");
        }
    }
    public static interface SevenSeater extends Vehicle {}

    public static interface SUV extends Vehicle {
        default void whatAmI() {
            System.out.println("I am a SUV");
        }
    }

    public static class TeslaModelX implements SevenSeater, SUV {
        public void identifyMyself() {
            whatAmI();
        }
    }

    public static void main( String args[] ) {
        (new TeslaModelX()).identifyMyself();

        // answer: https://nipafx.dev/java-default-methods-guide/
        // rule 1: Classes win over interfaces.
        // rule 2: More specific interfaces win over less specific ones (where specificity means "subtyping").
        //      A default from List wins over a default from Collection, regardless of where
        //      or how or how many times List and Collection enter the inheritance graph.
        // rule 3: or rather its absence, means that concrete classes must implement each method for
        //      which competing default implementations exist. Otherwise the compiler throws an error.
        //      If one of the competing implementations is appropriate, the method body can just explicitly call that method.
    }
}

class Question3 {
    public interface SuperPower {
        void fly();
        boolean equals(Object obj);
        int hashCode();
    }

    public static class JetPack {
        public void fly() {
            System.out.println("fly away");
        }
    }

    public static class FlyingMan extends JetPack implements SuperPower {
        void identify() {
            System.out.println("I am a flying man.");
        }
    }

    public static void main(String[] args) {

        // question: The class FlyingMan implements the interface SuperPower but doesn’t provide an implementation for it directly.
        // Will this code snippet compile since the class JetPack has a fly method?
        new FlyingMan().identify();
    }
}