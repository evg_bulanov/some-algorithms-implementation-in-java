package some.inverview.questions.inheritance;

public class Demonstration6 {
    public static void main(String[] args) {
        new Demonstration6.BiLingual().converse();
    }

    static interface Language {

        default void sayHello() {
            System.out.println("01101000 01100101 01101100 01101100 01101111 ");
        }
    }

    static interface Marathi extends Language{}

    static interface Punjabi extends Language {}

    static class BiLingual implements Punjabi, Marathi {void converse() {sayHello();}}
}
