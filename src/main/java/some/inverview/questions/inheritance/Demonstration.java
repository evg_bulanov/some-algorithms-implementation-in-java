package some.inverview.questions.inheritance;

/**
 * In object-oriented programming, inheritance enables new objects to take on the properties of existing objects.
 *  superclass (base class) -> subclass (derived class).
 *
 * benefits of inheritance - code reusability is the prime benefit of inheritance.
 *
 * polymorphism - poly means many and morph means shape or form.
 *  Polymorphism is thus, the ability in programming to present the same interface for differing underlying forms or data types.
                        +---------------+
                        | shape.draw()  |
                        +---------------+
                   +->         ^        <-+
                   |           |          |
           +------------+ +-------+  +-------------+
           | Circle     | | Rct   |  | triangle    |
           +-----------+ +-------+  +-------------+

 * Yoo can work with object by base interface / class without knowing details.
 *
 * !!! Java doesn't support multiple class inheritance. A derived class can only extend a single super class!
 * !!! The Java programming language supports multiple inheritance of type,
 *          which is the ability of a class to implement more than one interface.

 */



class Demonstration {
    public static void main( String args[] ) {
        Aircraft[] array = new Aircraft[3];
        array[0] = new BellCobraHelicopter();
        array[1] = new Boeing747();
        array[2] = new Falcon9Rocket();

        for(Aircraft aircraft : array){
            aircraft.fly();
        }
    }
}

abstract class Aircraft {
    public abstract void fly();
}

class BellCobraHelicopter extends Aircraft {
    public void fly() {
        System.out.println("chopper away ..");
    }
}

class Boeing747 extends Aircraft {
    public void fly() {
        System.out.println("Boeing takes off.");
    }
}

class Falcon9Rocket extends Aircraft {
    public void fly() {
        System.out.println("Rocket blasts off..");
    }
}


/**
 * diamond problem - Java avoids the diamond problem by disallowing multiple inheritance.
 */
 abstract class Language {
    public abstract void sayHello();
}

class Punjabi extends Language {
    String lang = "punjabi";

    public void sayHello() {
        System.out.println("Kiddaan");
    }
}

class Marathi extends Language {
    String lang = "marathi";

    public void sayHello() {
        System.out.println("Namaskaar");
    }
}

class BiLingual extends Punjabi
//        , Marathi  // multiple inheritance not allowed in java!
{
    public void greet() {
        super.sayHello();
    }
}