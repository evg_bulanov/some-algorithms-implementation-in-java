package some.inverview.questions.inheritance;

/**
 * Multipe inheritance for intefaces with default implementations
 */
public class Demonstration2 {
    public static void main(String[] args) {
        new MultiLingual().sayHello();
    }

    static interface Language {
        void sayHello();
    }

    static  interface Punjabi extends Language {
        String lang = "punjabi";

        default void sayHello() {
            System.out.println("O Kiddaan");
        }
    }

    static interface Marathi extends Language {
        String lang = "Marathi";
        default void sayHello() {
            System.out.println("Namaskaar");
        }
    }

    static class MultiLingual implements Punjabi, Marathi {

        // Must provide implementation for the sayHello
        // method, otherwise the code will not compile
        // even though both the interfaces provide for
        // default implementations
        public void sayHello() {
            System.out.println("I forgot how to say hello in both " + Marathi.lang  + " & " + Punjabi.lang);
        }
    }
}




