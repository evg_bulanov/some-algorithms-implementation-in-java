package some.inverview.questions.fctr.abstr;

public class AbstractFactoryMethodDemo {
    interface GPU {}
    static class MsiGPU implements GPU {}
    static class AsusGPU implements GPU {}

    interface Monitor {}
    static class MsiMonitor implements Monitor {}
    static class AsusMonitor implements Monitor {}

    static abstract class Company {
        abstract GPU createGPU();
        abstract Monitor createMonitor();
    }

    static class MsiManufacture extends Company {
        @Override
        GPU createGPU() {
            return new MsiGPU();
        }

        @Override
        Monitor createMonitor() {
            return new MsiMonitor();
        }
    }

    static class AsusManufacture extends Company {
        @Override
        GPU createGPU() {
            return new AsusGPU();
        }

        @Override
        Monitor createMonitor() {
            return new AsusMonitor();
        }
    }

    public static void main(String[] args) {
        Company msi = new AsusManufacture();
        GPU msiGpu = msi.createGPU();
        Monitor msiMonitor = msi.createMonitor();

        Company asus = new AsusManufacture();
        GPU asusGpu = asus.createGPU();
        Monitor asusMonitor = asus.createMonitor();
    }
}
