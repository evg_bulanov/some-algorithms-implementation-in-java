package some.inverview.questions.fctr.mthd;

/**
 * Default interface method as factory method pattern
 * https://www.youtube.com/watch?v=yTuwi--LFsM&t=4437s
 */
public class FactoryMethodAsDefaultDemo {
    interface Pet {}
    static class Dog implements Pet {}
    static class Cat implements Pet {}

    static class DogPerson implements Person {
        @Override
        public Pet getPet() {
            return new Dog();
        }
    }

    static class CatLover implements Person {
        @Override
        public Pet getPet() {
            return new Cat();
        }
    }

    interface Person {
        Pet getPet();
        default void play() {
            System.out.println("Playing with " + getPet());
        }
    }

    public static void call(Person person) {
        person.play();
    }
    public static void main(String[] args) {
        call(new DogPerson());
        call(new CatLover());
    }
}
