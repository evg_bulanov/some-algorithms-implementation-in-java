package some.inverview.questions.fctr.mthd;


/*
 Factory method

+------------------------+                    |-------------- |
| Creator                |                    | Product       |
|  (Restaurant)          |------------------->| interface     |
| + someOperation        |                    | +doStuff      |
|+ createProduct: Product| <------|           |---------------|
+------------------------+        |                 ^       ^
          ^                       |  |-------------------|  |
          |                       |  | ConcreteProductA  |  |
          |                       |  |-------------------|  |
 +---------------------------+    |                         |
 | ConcreteCreatorA          |    |                     |-------------------|
 |  (BeefRestaurant)         |    |                     | ConcreteProductA  |
 | + createProduct: Product  |    |                     |-------------------|
 +---------------------------+    |
                                  |
                               +---------------------------+
                               | ConcreteCreatorB          |
                               |  (VeggieRestaurant)       |
                               | + createProduct: Product  |
                               +---------------------------+

 Take a loot at great video at https://www.youtube.com/watch?v=EdFq_JIThqM
 */
public class FactoryMethodDemo {
    public static void main(String[] args) {
        BeefBurgerRestaurant beefBurgerRestaurant =  new BeefBurgerRestaurant();
        beefBurgerRestaurant.orderBurger();

        VeggieBurgerRestaurant veggieBurgerRestaurant = new VeggieBurgerRestaurant();
        veggieBurgerRestaurant.orderBurger();
    }

    interface Burger {
        void prepare();
    }

    static class BeefBurger implements Burger {
        @Override
        public void prepare() {
            System.out.println("Prepare beef");
        }
    }

    static class VeggieBurger implements Burger {
        @Override
        public void prepare() {
            System.out.println("Prepare veggie");
        }
    }

    static abstract class Restaurant {
        public Burger orderBurger() {
            Burger burger = createBurger();
            burger.prepare();
            return burger;
        }

        protected abstract Burger createBurger();
    }

    static class BeefBurgerRestaurant extends Restaurant {
        @Override
        protected Burger createBurger() {
            return new BeefBurger();
        }
    }

    static class VeggieBurgerRestaurant extends Restaurant {
        @Override
        protected Burger createBurger() {
            return new VeggieBurger();
        }
    }
}
