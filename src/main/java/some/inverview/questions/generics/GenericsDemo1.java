package some.inverview.questions.generics;

import java.util.ArrayList;
import java.util.List;

/**
 *  generics introduced in J2SE 1.5 in 2004
 *
 *  Generics help with the following use-cases
 *   - Enforcing stronger type checking at compile time for greater type-safety
 *   - Elimination of casts
 *   - Implementation of generic algorithms
 *
 * Generic type
 *   - is a generic class or interface that is parameterized over types.
 * For instance, the LinkedList<E> class in java is a generic class.
 *
 * The E in the generic class LinkedList<E> is called a type parameter.
 * By convention, type parameter names are single, uppercase letters.
 *      E - Element (used extensively by the Java Collections Framework)
 *      K - Key
 *      N - Number
 *      T - Type
 *      V - Value
 *      S, U, V - 2nd, 3rd, 4th types
 *
 * Pair of angle brackets <> is informally called the diamond notation.
 *
 */
public class GenericsDemo1 {
    public static void main(String[] args) {
        // example with diamond notation
        List<String> names = new ArrayList<>();

        // instead of the more verbose
        // example without diamond notation
        List<String> names2 = new ArrayList<String>();

        // type safe problem
        ArrayList myList = new ArrayList();

        myList.add("Hello");
        myList.add(2);

//        sum(myList); // exception class java.lang.String cannot be cast to class java.lang.Integer
        sum(myList); // exception class java.lang.String cannot be cast to class java.lang.Integer
    }

    static int sum(ArrayList list) {

        int sum = 0;
        for (int i = 0; i < list.size(); i++)
            sum += (int) list.get(i);

        return sum;
    }
}
