package some.inverview.questions.generics;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class GenericBridgeDemo {
    interface UselessInterface<T> {
        void print(T o);
    }

    static class PurposelessClass implements UselessInterface<Integer> {
        @Override
        public void print(Integer o) {

        }
    }

    static class WorkOnListOfInts<T extends List<Integer>> {
        public void work(T ints) {
            for (Integer x : ints) {
                System.out.println(x);
            }
        }
    }

    public static void main(String[] args) {
        // two "print" methods, real and bridge
        // public void some.inverview.questions.generics.GenericBridgeDemo$PurposelessClass.print(java.lang.Integer)
        // public void some.inverview.questions.generics.GenericBridgeDemo$PurposelessClass.print(java.lang.Object)

        for (Method m : PurposelessClass.class.getMethods()) {
            if (m.getName().equals("print")) {
                System.out.println(m.toGenericString());
            }
        }

        // --------------

        for (Method m : WorkOnListOfInts.class.getMethods()) {
            if (m.getName().equals("work")) {
                System.out.println(m.toGenericString());
            }
        }

        // will not compile
        // WorkOnListOfInts<List<Integer>> worker = new WorkOnListOfInts<>();
        // List<Double> doubleList = Arrays.asList(4.4);
        // worker.work(doubleList);

        // will get runtime exception
        WorkOnListOfInts<List<Integer>> worker = new WorkOnListOfInts<>();
        List<Double> doubleList = Arrays.asList(4.4);
        List objList = doubleList;
        worker.work(objList);
    }
}
