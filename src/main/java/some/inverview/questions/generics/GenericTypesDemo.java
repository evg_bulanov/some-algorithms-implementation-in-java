package some.inverview.questions.generics;

/**
 *  Class or an interface can be parameterized over types and is considered generic.
 */
public class GenericTypesDemo {
    /** Example of generic type */
    static class Printer<T> {
        void print(T t) {
            System.out.println("printing : " + t.toString());
        }
        /** example of generic method */
        <D> void doSomething(D param) {
            System.out.println("Do some logic here with " + param);
        }

    }

    public static void main(String[] args) {
        new Printer<Integer>().print(35);

        // ex of generic method in generic type
        new Printer<Integer>().<String>doSomething("test value");
    }
}
