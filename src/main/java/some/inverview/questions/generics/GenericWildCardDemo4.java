package some.inverview.questions.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericWildCardDemo4 {
    public static void main( String args[] ) {
        System.out.println( "Hello World!" );
    }

    <T extends Tiger> T methodWillCompile(T someAnimal){
        return null;
    }

    // super can be used in return type
//    <T super Tiger> T methodWontCompile(T someAnimal){
//        return null;
//    }

    static class Animal {

        void speakUp() {
            System.out.println("gibberish");
        }
    }

    static class Tiger extends Animal {

        @Override
        void speakUp() {
            System.out.println("Rooaaarrssss");
        }
    }
}