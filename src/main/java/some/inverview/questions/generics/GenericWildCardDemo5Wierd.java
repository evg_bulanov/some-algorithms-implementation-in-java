package some.inverview.questions.generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Put and get
 * https://stackoverflow.com/questions/4343202/difference-between-super-t-and-extends-t-in-java
 */
public class GenericWildCardDemo5Wierd {
    public static void main(String[] args) {

        List<? extends  Number> listOfNumbers = new ArrayList<>();
        Integer i = Integer.valueOf(5);
        // Attempting to put an integer in a list of uknown types that extend Number
        // listOfNumbers.add(i); // <--- compile time error

        List<Number> list = new ArrayList<>();
        Integer i2 = Integer.valueOf(5);
        // Attempting to put an integer in a list of Number
        list.add(i2); // <--- compiles

        // The reason, we can't insert an integer into a list of type ? extends T is because we could end up storing,
        // for example a double into a list of integers as shown below

        List<Integer> listOfInts = new ArrayList<>();
        List<? extends Number> reference = listOfInts; // Allowed
//        reference.add(new Double(4.0)); // <--- compile time error

        /**
         * The put principle is the inverse of the get principle. A list that with the bound List<? super T> can have
         * elements of type T or its subtypes added to it but never taken out. The only value you can take out of such
         * a list is of type Object since that is the supertype of every type. The put principle says to use a super
         * wildcard when you only want to put values into a structure.
         */

        List<? super Number> listOfNumbers2 = new ArrayList<>();
        Integer i3 = Integer.valueOf(5);
        Double d = Double.valueOf(5);

        // Adding an integer
        listOfNumbers2.add(i3); // Allowed
        // Adding a double
        listOfNumbers2.add(d); // Allowed

//        i = listOfNumbers2.get(0); // <--- Compile time error

        Object object = listOfNumbers2.get(0); // Allowed
    }

    // The above code will never compile because we are trying to add a number into a list of a type that extends Number
    // and we can't guarantee the type of the number being passed into the method.
    void addNumber(Number num) {
        List<? extends Number> listOfNumbers = new ArrayList<>();
//        listOfNumbers.add(num); // Will this work?
    }
}
