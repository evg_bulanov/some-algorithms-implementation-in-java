package some.inverview.questions.generics;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class GenericTypeInference {
    public static class InferenceExample {
        public <U> U identity(U item) {
            return item;
        }
    }

    public static void main(String[] args) {
        InferenceExample inferenceExample = new InferenceExample();
        double g = inferenceExample.<Double>identity(45d);

        // generic type inference is alibily of compiler to identify type
        // example, see that generic type of the method is not provided
        InferenceExample inferenceExample1 = new InferenceExample();
        double result = inferenceExample1.identity(45d);

        // without <> - raw type is used, with <> compiler can identify type
        HashMap<Integer, Integer> map = new HashMap();

        // target type of an expression is the data type that the Java compiler expects
        //      depending on where the expression appears.
        //
        //        public static final <T> List<T> emptyList() {
        //            return (List<T>) EMPTY_LIST;
        //        }
        List<String> stringList = Collections.emptyList();

    }
}
