package some.inverview.questions.generics;

/**
 * Static inner class or, for that matter, static methods, can't access the parametrized type of the containing class.
 */
public class GenericStaticNestedClassesDemo {
    static class OuterClass<T extends Comparable<T>> {
        T someField;

//        public static class InnerClass<V> {
//            T itemInNestedClass = null; // <--- compile time error
//        }

//        static void outerClassStaticMthd() {
//            T item = null; // <--- compile time error
//        }

        // creating an object of the inner class parametrized on T
//        OuterClass.InnerClass<T> item;
    }
}
