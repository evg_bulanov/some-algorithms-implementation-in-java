package some.inverview.questions.generics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GenericBoundedTypesDemo {
    static class NumberCollection<T> {

        List<T> list = new ArrayList<>();

        // Method compares integer portions of each
        // value stored in the list and prints those
        // that are greater than the method argument
        public void printGreater(T other) {
            for (T item : list) {
                // crude way to get integer portions
                int val1 = Double.valueOf(item.toString()).intValue();
                int val2 = Double.valueOf(other.toString()).intValue();
                if (val1 > val2)
                    System.out.println(item);
            }
        }

        public void add(T item) {
            list.add(item);
        }
    }

    static class NumberCollectionBounded<T extends Number> {
        List<T> list = new ArrayList<>();

        // Print if the integer portion is greater
        public void printGreater(T other) {
            for (T item : list) {
                if (item.intValue() > other.intValue())
                    System.out.println(item);
            }
        }

        public void add(T item) {
            list.add(item);
        }
    }

    /**
     *  type parameter can have multiple bounds, but it can be bounded by at most one class and as many interfaces as desired.
     *  The class must be mentioned first in the list of bounding types.
     */
    class NumberCollectionBoundedMultiple<T extends Number & Comparable & Serializable> {
        // ... class body
    }

    public static void main(String[] args) {
        // work well
        NumberCollection<Integer> myIntegerList = new NumberCollection<>();
        myIntegerList.add(5);
        myIntegerList.add(4);
        myIntegerList.printGreater(4);

        // does not work
//        NumberCollection<String> myStringList = new NumberCollection<>();
//        myStringList.add("hello");
//        myStringList.printGreater("world");

        // solution <T extends Number>
        NumberCollection<NumberCollectionBounded> myStringList = new NumberCollection<>();
//        myStringList.add("hello"); <-- compilation error
//        myStringList.printGreater("world"); <-- compilation error
    }
}

