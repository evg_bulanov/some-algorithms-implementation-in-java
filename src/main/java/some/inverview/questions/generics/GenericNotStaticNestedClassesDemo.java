package some.inverview.questions.generics;

/**
 * If the outer class has type parameters and the inner class is not static,
 * then type parameters of the outer class are visible within the inner class.
 */
public class GenericNotStaticNestedClassesDemo {
    static class OuterClass<T extends Comparable<T>> {
        T someField;

        public class InnerClass<V extends Comparable<V>> {
            T itemInNestedClass = null; // <--- parameterized type is accessible

            // creating an object of inner class with the outer
            // class parametrized on the type parameter of inner
            // class V, while the inner class is parameterized on
            // the type parameter of the outer class T
            OuterClass<V>.InnerClass<T> item2 = null;
        }

        // creating an object of the inner class parametrized on T
        OuterClass<T>.InnerClass<Integer> item;
    }
}
