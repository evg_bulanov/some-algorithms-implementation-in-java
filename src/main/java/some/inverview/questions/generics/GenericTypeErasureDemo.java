package some.inverview.questions.generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Erasure - enforcing type constraints only at compile time
 *      and discarding type information at runtime - or erasing type information.
 *
 * Generics in Java are implemented using erasure which has the following effects on the code:
 *      - replaces all type parameters (T, U, V, etc) in generic types (classes, interfaces, etc).
 *      - adds implicit casts to preserve type safety. Before generics, the casts were explicitly added by the developer
 *      - generates additional methods called bridge methods to preserve polymorphism in extended generic types.
 */

/**
 * difference between Collections and Array types in Java
 *
 * Arrays are covariant, that is an array of type person is a subtype of an array of objects.
 * However, collections are invariant that is a list of type person is not a subtype of list of objects.
 *
 * Arrays are reified, that is they know the type of their components at runtime and enforce it.
 * One can't store a String type in an array of Double. On the other hand, Collections enforce their
 * type constraints only at compile time and erase element type information at runtime.
 * Erasure is what allows generic types to interoperate freely with legacy code that does not use generics.
 *
 * Collections are more flexible than arrays. With arrays we are limited to setting or retrieving a particular index.
 * However, with collections we have a variety of operations that can be performed such as sorting, shuffling,
 * rotation combining, extracting a sublist, etc.
 *
 * Collections allow us to imply an ordering of the constituent elements when using lists or not at all using sets.
 *
 * Arrays can be efficient especially when composed of primitive types. Because primitives don't have subtypes,
 * the array doesn't need to check for a ArrayStoreException. Primitive array types also avoid the performance penalty of boxing.
 * Arrays can be used in performance crucial situations in preference to collections.
 */
public class GenericTypeErasureDemo {
    public static void main( String args[] ) {
        // Raw type
        ArrayList al = new ArrayList();

        // ArrayList of Strings
        ArrayList<String> strs = new ArrayList<>();

        // ArrayList of ArrayList of Strings
        ArrayList<ArrayList<String>> lstOfStringLsts = new ArrayList<>();

        System.out.println("al.getClass().equals(strs.getClass()) = " + al.getClass().equals(strs.getClass()));
        System.out.println("al.getClass().equals(lstOfStringLsts.getClass()) = " + al.getClass().equals(lstOfStringLsts.getClass()));

        /**
         * !!!
         * It is important to realize that the types ArrayList<Integer>, ArrayList<String>, and ArrayList<List<Integer>>
         *     are all represented at run-time by the same type, ArrayList
         */

        /**
         * Erasure rules on the above methods.
         *      - discard all type parameters from parameterized types: we can remove T wherever we see it.
         *      - replace any type variable with the erasure of its bound or with Object if it has no bound
         *      - if multiple bounds exist, pick the erasure of the leftmost or first bound.
         */
    }

    static class BeforeErase {
        // Prints Integers
//        <T extends List<Integer>> void print(T lst) {
//            for (Integer i : lst)
//                System.out.println(i);
//        }

        // Prints Strings
//        <T extends List<String>> void print(T lst) {
//            for (String str : lst)
//                System.out.println(str);
//        }

// method above will not compile because has same erasure
        // Prints Objects
        <T extends List> void print(T lst) {
            for (Object str : lst)
                System.out.println(str);
        }
    }

    static class AfterErasure {
        // Prints Integers
//        void print(List lst) {
//
//            for (Integer i : lst)
//                System.out.println(i);
//        }
//
//        // Prints Strings
//        void print(List lst) {
//            for (String str : lst)
//                System.out.println(str);
//        }
//
//        // Prints Objects
//        void print(List lst) {
//            for (Object str : lst)
//                System.out.println(str);
//        }
    }

    // A class representing a box of some type
    static class Box<T> {
        T item;

        public Box(T givenItem) {
            item = givenItem;
        }
    }

    // A class representing a box of some type
    static class BoxObject {
        Object item;

        public BoxObject(Object givenItem) {
            item = givenItem;
        }
    }

    static class BoxComparable<T extends Comparable<T>> {

        T item;

        public BoxComparable() {

        }

        public int compareTo(T other) {
            return item.compareTo(other);
        }
    }
}
