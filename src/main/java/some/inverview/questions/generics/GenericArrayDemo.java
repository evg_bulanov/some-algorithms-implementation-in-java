package some.inverview.questions.generics;

public class GenericArrayDemo {
    // Method to create and return arrays
    // doesn't compile
//    <T> T[] createArray(int size) {
//        T[] array = new T[size]; // <--- compile error ( you can't create an array of a parameterized type i.e. you can't do new T[100])
//        return array;
//    }

    // buy you can
    <T> void someMethod(T... args) {
        // Create a reference for the array of parameterized type
        // and assign it the arguments
        T[] input = args;
        for (T t : input)
            System.out.println(t.toString());
    }

    //    naive and incorrect way is to create an array of a generic type using casting
    // The above code will compile but throws a ClassCastException when run.
    // Note that the compilation also issues an unchecked warning.
    // The runnable snippet below exemplifies this.
    <T> T[] createArray(int size) {
        T[] array = (T[]) new Object[size];
        return array;
    }

    // the right way of creating array of generic is reflection
    <T> T[] createGenericArray(Class<T> type, int size) {
        T[] arr = (T[]) java.lang.reflect.Array.newInstance(type, size);
        return arr;
    }

    public static void main(String[] args) {
//        String aar[] = new GenericArrayDemo().<String>createArray(5);
         //class [Ljava.lang.Object; cannot be cast to class [Ljava.lang.String; ([Ljava.lang.Object; and [Ljava.lang.String; are in module java.base of loader 'bootstrap')

        String aar[] = new GenericArrayDemo().<String>createGenericArray(String.class, 5);
    }
}
