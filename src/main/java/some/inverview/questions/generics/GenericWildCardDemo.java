package some.inverview.questions.generics;

import java.util.ArrayList;
import java.util.Collection;

/**
 * , the question mark, ?, called the wildcard, represents an unknown type.
 * The wildcard can be used in a variety of situations: as the type of a parameter, field,
 *      or local variable and sometimes even as a return type.
 *
 * Note that the wildcard can only be used in defining arguments to a method in a method signature.
 *      It can't appear in the return type expression.
 */
public class GenericWildCardDemo {
    static class Animal {
        void speakUp() {
            System.out.println("gibberish");
        }
    }

    static class Elephant extends Animal {
        @Override
        public void speakUp() {
            System.out.println("Trumpets..");
        }
    }

    static class Tiger extends Animal {
        @Override
        void speakUp() {
            System.out.println("Rooaaarrssss");
        }
    }

    public static void main( String args[] ) {
        Collection<Tiger> tigers = new ArrayList<>();
        tigers.add(new Tiger());
        printAnimal(tigers);
    }

    static void printAnimal(Collection<? extends Animal> animals) {
        for (Animal animal : animals)
            animal.speakUp();
    }

    static <T extends Animal> void printAnimal2(Collection<T> animals) {
        for (Animal animal : animals)
            animal.speakUp();
    }
}
