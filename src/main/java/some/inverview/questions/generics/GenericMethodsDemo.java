package some.inverview.questions.generics;

import org.springframework.http.converter.GenericHttpMessageConverter;

/**
 * Generic methods are methods that introduce their own type parameters.
 *
 * This is similar to declaring a generic type, but the type parameter's scope is limited to the method where it is declared.
 * The scope of the type variable is local to the method itself.
 * it may appear in the method signature and the method body, but not outside the method.
 *
 * A generic method can appear in a class which itself is not generic.
 * The syntax for a generic method includes a list of type parameters, inside angle brackets, which appears before the method's return type. B
 */
public class GenericMethodsDemo {

    public <I> void genericMethod(I input) {
        System.out.println(input);
    }

    public static void main(String[] args) {
        GenericMethodsDemo gm = new GenericMethodsDemo();
        gm.<Integer>genericMethod(5);
        gm.<String>genericMethod("5");
        gm.genericMethod(5.23f);

    }
}
