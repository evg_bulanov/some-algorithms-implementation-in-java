package some.inverview.questions.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericWildCardDemo3 {
    public static void main( String args[] ) {
        List<Animal> animals = new ArrayList<>();

        // The method is able to accept a list of superclass of Tiger
        processAnimals(animals);

        // uncommenting the line below will show a compile error
        // unableToProcessAnimals(animals);
    }

    static void processAnimals(List<? super Tiger> list) {
        // Your awesome code here
    }

    static void unableToProcessAnimals(List<Tiger> list) {
        // Your awesome code here
    }
    static class Animal {

        void speakUp() {
            System.out.println("gibberish");
        }
    }

    static class Tiger extends Animal {
        @Override
        void speakUp() {
            System.out.println("Rooaaarrssss");
        }
    }
}