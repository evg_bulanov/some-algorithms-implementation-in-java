package some.inverview.questions.generics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GenericWildCardDemo2 {
    public static void main( String args[] ) {
        List<Object> list = new ArrayList<>();
//        list.add(new Integer(5));
//        list.add(new Double(77.23));
//        list.add("randomString");
        notVeryUsefulMethod(list);
    }

    static void notVeryUsefulMethod(Collection<?> collection) {
        for (Object obj : collection)
            System.out.println(obj.getClass());
    }
}
