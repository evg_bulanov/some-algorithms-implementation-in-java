package some.inverview.questions.generics;


public class GenericsDemo2 {
    public static class Printer<T> {
        T item;

        public Printer(T item) {
            this.item = item;
        }

        public void consolePrinter() {
            System.out.println(item.toString());
        }

        public void changeItem(T item) {
            this.item = item;
        }
    }

    public static void main(String[] args) {
        // The snippet Printer printer = new Printer(5); will compile just fine.
        // When the actual type parameter is omitted, a raw type is created.
        Printer p = new Printer(5);
        p.consolePrinter();

        p = new Printer("5");
        p.consolePrinter();

        Printer p2 = new Printer(5);
        p2.changeItem("4");
        p2.consolePrinter();
    }
}
