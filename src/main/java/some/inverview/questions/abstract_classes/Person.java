package some.inverview.questions.abstract_classes;

/**
 *  An abstract class
 *   - can't be instantiated, but it can be subclassed,
 *   - contains abstract and non-abstract methods that subclasses are forced to provide an implementation for,
 *   - Non-abstract methods will also have a method body that serves as the default implementation,
 *   - if a class includes abstract methods, then the class itself must be declared abstract.
 *
 */
public abstract class Person implements Comparable<Person> {
    int age;
    String firstName;
    String lastName;
    String middleName;

    public Person() {
    }

    public Person(int age, String firstName, String middleName, String lastName) {
        this.age = age;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    // Abstract method must be implemented by the subclass.
    abstract void printFullName();

    // Default implementation for defaultAge
    void printAge() {
        System.out.println();
    }
}