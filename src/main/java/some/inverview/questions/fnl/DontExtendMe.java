package some.inverview.questions.fnl;

/**
 *  final class an't be extended or inherited from.
 *  The String class is one such example.
 */
final class DontExtendMe {

}

// Compile time error
//class WontCompile extends DontExtendMe {
//
//}