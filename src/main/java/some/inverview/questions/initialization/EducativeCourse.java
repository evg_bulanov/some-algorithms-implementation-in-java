package some.inverview.questions.initialization;

/**
 * { }, and preceded by the static keyword.
 * used to initialize static fields of a class,
 * can have any number of static initialization blocks,
 * can appear anywhere in the class,
 * called in the order that they appear in the source code.
 */
public class EducativeCourse {
    static String courseName;
    static String version;

    // We have two static initialization blocks
    static {
        version = "1.0";
        System.out.println("static block 1");
    }

    static {
        courseName = "Java Interview Bible";
        System.out.println("static block 2");
    }

    public static void main(String[] args) {
        System.out.println("main");
    }

// result:
//    static block 1
//    static block 2
//    main
}
