package some.inverview.questions.initialization;

class Demonstration {
    public static void main( String args[] ) {
        InitializationBlock ec = new InitializationBlock();
        System.out.println( "Course name: " + ec.courseName );
    }
}

class InitializationBlock {
    String courseName = setCourseName();
    String version;
    // initialization block
    {
        version = "1.0";
    }

    // final method used for intialization
    private String setCourseName() {
        return "Java Interview Bible";
    }
}
