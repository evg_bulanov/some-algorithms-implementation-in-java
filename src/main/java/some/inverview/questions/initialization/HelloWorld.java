package some.inverview.questions.initialization;

public class HelloWorld {
    static final int[] array = new int[5];

    public static void main( String args[] ) {
        changeArrayContents(0, 9);
    }

    // Allowed
    static void changeArrayContents(int  i, int val) {
        array[i] = val;
    }

    // Not allowed and will not compile
    /*
    static void changeArray(){
        array = new int[10];
    }*/
}
