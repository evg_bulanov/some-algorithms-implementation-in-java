package some.inverview.questions.process;

/**
 * The thread class exposes the interrupt() method which can be used to interrupt a thread that is blocked
 * in a sleep() or wait() call. Note that invoking the interrupt method only sets a flag that is polled periodically by
 * sleep or wait implementation to know if the current thread has been interrupted. If so an interrupted exception is thrown.
 */
public class InterruptDemo {
    public static void main( String args[] ) throws InterruptedException {
        InterruptExample.example();
    }

    static class InterruptExample {
        static public void example() throws InterruptedException {
            final Thread sleepyThread = new Thread(() -> {
                try {
                    System.out.println("I am too sleepy... Let me sleep for an hour.");
                    Thread.sleep(1000 * 60 * 60);
                } catch (InterruptedException ie) {
                    // Once the interrupted exception is thrown, the interrupt status/flag is cleared as the output of line-19 shows.
                    System.out.println("The interrupt flag is cleared : " + Thread.interrupted() + " " + Thread.currentThread().isInterrupted()); // false false

                    // we again interrupt the thread and no exception is thrown. This is to emphasize that merely calling
                    // the interrupt method isn't responsible for throwing the interrupted exception.
                    // Rather the implementation should periodically check for the interrupt status and take appropriate action.
                    Thread.currentThread().interrupt();
                    System.out.println("Oh someone woke me up ! ");
                    System.out.println("The interrupt flag is set now : " + Thread.currentThread().isInterrupted() + " " + Thread.interrupted()); // true true

                    // Note that there are two methods to check for the interrupt status of a thread.
                    // One is the static method Thread.interrupted() and the other is Thread.currentThread().isInterrupted().
                    // The important difference between the two is that the static method would return the interrupt status
                    // and also clear it at the same time.
                    //
                    // On line 28 we deliberately call the object method first followed by the static method.
                    // If we reverse the ordering of the two method calls on line 28, the output for the line would be
                    // true and false, instead of true and true.
                }
            });

            sleepyThread.start();
            Thread.sleep(100);
            System.out.println("About to wake up the sleepy thread ...");
            sleepyThread.interrupt();
            System.out.println("Woke up sleepy thread ...");

            sleepyThread.join();
        }
    }
}
