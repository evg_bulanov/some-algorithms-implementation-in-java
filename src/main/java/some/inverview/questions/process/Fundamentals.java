package some.inverview.questions.process;

/**
 * Some differences between a process and a thread are:
 *      - a process can have many threads, whereas a thread can belong to only one process.
 *      - a thread is lightweight than a process and uses less resources than a process.
 *      - a thread has some state private to itself but threads of a process can share the resources allocated to the process
 *        including memory address space.
 *
 * Threads used without thought can sometimes lead to performance degradation for the following reasons:
 *      - Usually hard to find bugs (some that may only rear head in production environments (think race conditions))
 *      - higher cost of code maintenance (since the code inherently becomes harder to reason about)
 *      - increased utilization of system resources (Creation of each thread consumes additional memory,
 *              CPU cycles for book-keeping and waste of time in context switches).
 *      - too many threads can decrease program performance due to increased competition to acquire locks (lock contention).
 *
 * Liveness - Ability of a program or an application to execute in a timely manner is called liveness.
 *      If a program experiences a deadlock then it's not exhibiting liveness.
 *
 * Starvation
 *      an application thread can also experience starvation, where the thread never gets CPU time or access to shared resources
 *      because other "greedy" threads hog system resources.
 *
 *
 */
public class Fundamentals {
}
