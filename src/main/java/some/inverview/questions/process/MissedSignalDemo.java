package some.inverview.questions.process;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A missed signal happens when a signal is sent by a thread before the other thread starts waiting on a condition.
 * This is exemplified by the following code snippet. Missed signals are caused by using the wrong concurrency constructs.
 * In the example below, a condition variable is used to coordinate between the signaller and the waiter thread.
 * The condition is signaled at a time when no thread is waiting on it causing a missed signal.
 */
public class MissedSignalDemo {

    public static void main(String[] args) throws InterruptedException {
//        missingSignalDemo();
        // The above code when ran, will never print the statement Program Exiting and execution would time out.
        // The fix is to use a semaphore for signalling between the two thread as shown below
        fixedMissingSignalDemo();
    }

    public static void missingSignalDemo() throws InterruptedException {
        final ReentrantLock lock = new ReentrantLock();
        final Condition condition = lock.newCondition();

        Thread signaller = new Thread(() -> {
            lock.lock();
            condition.signal();
            System.out.println("Sent signal");
            lock.unlock();
        });

        Thread waiter = new Thread(() -> {
            lock.lock();
            try {
                condition.await();
                System.out.println("Received signal");
            } catch (InterruptedException ie) {
                // handle interruption
            }
            lock.unlock();

        });

        signaller.start();
        signaller.join();

        waiter.start();
        waiter.join();

        System.out.println("Program Exiting.");
    }

    public static void fixedMissingSignalDemo() throws InterruptedException {
        final Semaphore semaphore = new Semaphore(1);

        Thread signaller = new Thread(() -> {
            semaphore.release();
            System.out.println("Sent signal");
        });

        Thread waiter = new Thread(() -> {
            try {
                semaphore.acquire();
                System.out.println("Received signal");
            } catch (InterruptedException ie) {
                // handle interruption
            }
        });

        signaller.start();
        signaller.join();
        Thread.sleep(5000);
        waiter.start();
        waiter.join();

        System.out.println("Program Exiting.");
    }
}
