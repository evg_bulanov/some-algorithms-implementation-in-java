package some.inverview.questions.process;

import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A mutex allows only a single thread to access a resource.
 * It forces competing threads to serialize their access for the requested resource.
 *
 * A semaphore can potentially act as a mutex if the number of permits it can give out is set to 1.
 * However, the most important difference between the two is that in the case of a mutex the same thread must call acquire
 * and subsequent release on the mutex whereas in case of a binary sempahore,
 * different threads can call acquire and release on the semaphore.
 *
 * This leads us to the concept of "ownership".
 * A mutex is owned by the thread acquiring it, till the point, it releases it, whereas for a semaphore there's no notion of ownership.
 *
 * See for additional info:
 * https://www.baeldung.com/java-mutex
 */
public class MutexDemo {
    private static int counter = 0;
    private static Object mutex = new Object();
    private static ReentrantLock mutexReentrantLock = new ReentrantLock();
    private static Semaphore mutexBySemaphore = new Semaphore(1);

    /**
     * Implement mutex by synchronized
     */
    public static void mutexJobSynchronized() {
        while (counter < 1000) {
            synchronized (mutex) {
                counter++;
                System.out.println("Thread [" + Thread.currentThread().getName() + "], counter = " + counter);
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * The ReentrantLock class was introduced in Java 1.5.
     * It provides more flexibility and control than the synchronized keyword approach.
     */
    public static void mutexJobReentrantLock() {
        while (counter < 1000) {
            try{
                mutexReentrantLock.lock();
                counter++;
                System.out.println("Thread [" + Thread.currentThread().getName() + "], counter = " + counter);
            } finally {
                mutexReentrantLock.unlock();
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * While in case of a mutex only one thread can access a critical section, Semaphore allows a fixed number of
     * threads to access a critical section. Therefore, we can also implement a mutex by setting the number of
     * allowed threads in a Semaphore to one.
     */
    public static void mutexBySemaphore1() {
        while (counter < 1000) {
            try {
                mutexBySemaphore.acquire();
                counter++;
                System.out.println("Thread [" + Thread.currentThread().getName() + "], counter = " + counter);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                mutexBySemaphore.release();
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /** Monitor class of Google's Guava library is a better alternative to the ReentrantLock class.
     * As per its documentation, code using Monitor is more readable and less error-prone than the code using ReentrantLock.
     */
    public static void mutexByMonitorGuava() {
//        mutex.enter();
//        try {
//            sync logic here
//        } finally {
//            mutex.leave();
//        }
    }


    public static void main(String[] args) {
//        List<Thread> threads = List.of(new Thread(MutexDemo::mutexJobSynchronized), new Thread(MutexDemo::mutexJobSynchronized), new Thread(MutexDemo::mutexJobSynchronized));
//        threads.forEach(x -> x.start());

//        List<Thread> rlThreads = List.of(new Thread(MutexDemo::mutexJobReentrantLock), new Thread(MutexDemo::mutexJobReentrantLock), new Thread(MutexDemo::mutexJobReentrantLock));
//        rlThreads.forEach(x -> x.start());

        List<Thread> rlThreads = List.of(new Thread(MutexDemo::mutexBySemaphore1), new Thread(MutexDemo::mutexBySemaphore1), new Thread(MutexDemo::mutexBySemaphore1));
        rlThreads.forEach(x -> x.start());
    }
}
