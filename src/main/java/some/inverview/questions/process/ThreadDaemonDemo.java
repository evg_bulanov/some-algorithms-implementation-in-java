package some.inverview.questions.process;

/**
 * Daemon threads are low-priority threads whose only role is to provide services to user threads.
 *
 * Since daemon threads are meant to serve user threads and are only needed while user threads are running,
 * they won't prevent the JVM from exiting once all user threads have finished their execution.
 */
public class ThreadDaemonDemo {
    public void spawnThread() {
        Thread innerThread = new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 100; i++) {
                    System.out.println("I am a new thread [" + i + "]!" );
                }
            }
        });

        innerThread.setDaemon(true);
        innerThread.start();
        System.out.println("Main thread exiting");
    }

    public static void main(String[] args) {
        new ThreadDaemonDemo().spawnThread();
    }
}
