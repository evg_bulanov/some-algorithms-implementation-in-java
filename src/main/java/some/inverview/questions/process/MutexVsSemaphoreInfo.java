package some.inverview.questions.process;

/**
 * 1. Mutex implies mutual exclusion and is used to serialize access to critical sections
 *      whereas semaphore can potentially be used as a mutex, but it can also be used for cooperation
 *      and signalling amongst threads. Semaphore also solve the issue of 'missed signals'.
 *
 * 2. Mutex is 'owned' by a thread, whereas a semaphore has no concept of ownership.
 *
 * 3. Mutex if locked, must necessarily be unlocked by the same thread.
 *      A semaphore can be acted upon by different threads. This is true even if semaphore has a permit of one.
 *
 * 4. Think of semaphore analogous to a car rental service such as Hertz.
 *      Each outlet has a certain number of cars, it can rent out to customers.
 *      It can rent several cars to several customers at the same time but if all the cars are rented out
 *      then any new customers need to be put on a waitlist till one of the rented cars is returned.
 *
 *      In contrast, think of a mutex like a lone runway on a remote airport.
 *      Only a single jet can land or take-off from the runway at a given point in time.
 *      No other jet can use the runway simultaneously with the first aircraft.
 */
public class MutexVsSemaphoreInfo {
}
