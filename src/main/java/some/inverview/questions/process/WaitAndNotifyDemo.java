package some.inverview.questions.process;

/**
 *
 * https://www.baeldung.com/wp-content/uploads/2018/02/Java_-_Wait_and_Notify.png
 *
 * Object.wait() to suspend a thread
 * Object.notify() to wake a thread up
 *
 * Calling wait() forces the current thread to wait until some other thread invokes notify() or notifyAll() on the same object.
 *
 * The current thread must own the object's monitor.
 *  - when we've executed synchronized instance method for the given object
 *  - when we've executed the body of a synchronized block on the given object
 *  - by executing synchronized static methods for objects of type Class
 *
 * The overloaded methods
 *  - wait()
 *  - wait(long timeout)
 *  - wait(long timeout, int nanos)
 *
 *
 * The standard idiom for using the wait() method
 *   synchronized (obj) {
 *       while (<condition does not hold>)
 *           obj.wait(); // Lock released & reacquired on wakeup
 *           // ...         Do Processing
 *   }
 *
 * It is necessary to wrap the wait() method call in a while loop for the following reasons:
 *  - notifyAll() will wake up all the threads waiting on the same monitor.
 *    In situations where only one thread should be allowed to make progress,
 *    the rest of the threads should check for the invariant again in a while loop.
 *    It is possible that a previously woken up thread has changed the invariant back to false.
 *  - Spurious wakeups are possible, where a waiting thread can wake up without being interrupted or notified.
 *    The woken up thread should check the invariant again which will be false and go back to waiting on the monitor.
 *    This is only possible in a while loop
 *
 * Notify()
 *
 * Thread will not be scheduled for execution immediately and will compete with other active threads
 * that are trying to synchronize on the same object.
 * The thread which executed notify will also need to give up the object’s monitor,
 * before any one of the competing threads can acquire the monitor and proceed forward.
 *
 * NotifyAll()
 *
 * This method is the same as the notify() one except that it wakes up all the threads that are waiting
 * on the object's monitor.
 *
 */
public class WaitAndNotifyDemo {
    static boolean flag = false;

    public static void main(String args[]) throws InterruptedException {
        Object lock = new Object();
        Thread thread = new Thread(() -> {
            System.out.println("thread 1 start");
            synchronized (lock) {
                System.out.println("thread 1 sync start");
                while (!flag) {
                    try {

                        System.out.println("thread 1 is about to wait");
                        lock.wait();
                        System.out.println("thread 1 woken up with flag set to " + flag);
                    } catch (InterruptedException ie) {
                        // swallow
                    }
                }
            }
            System.out.println("thread 1 end");
        });

        thread.start();

        Thread.sleep(1000);

        Thread thread2 = new Thread(() -> {
            System.out.println("thread 2 start");
            synchronized (lock) {
                System.out.println("thread 2 sync start");
                while (!flag) {
                    lock.notify();
                    System.out.println("thread 2 just notified");
                    flag = true;
                }
            }


//             try {
//                 Thread.sleep(100);
//             } catch (InterruptedException ie){
//               // swallow
//             }

            synchronized (lock) {
                flag = false;
                System.out.println("thread 2 changed flag back to false");
            }
            System.out.println("thread 2 end");
        });

        thread2.start();
        thread2.join();
        thread.join();
    }
}
