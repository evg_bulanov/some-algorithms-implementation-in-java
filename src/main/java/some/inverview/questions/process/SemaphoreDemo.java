package some.inverview.questions.process;

/**
 * Semaphore is used for limiting access to a collection of resources.
 * Think of semaphore as having a limited number of permits to give out.
 * If a semaphore has given out all the permits it has, then any new thread that comes along requesting for a permit will be blocked,
 * till an earlier thread with a permit returns it to the semaphore.
 *
 * A typical example would be a pool of database connections that can be handed out to requesting threads.
 * Say there are ten available connections but 50 requesting threads.
 * In such a scenario, a semaphore can only give out ten permits or connections at any given point in time.
 *
 * A semaphore with a single permit is called a binary semaphore and is often thought of as an equal of a mutex,
 * which isn't completely correct. Semaphores can also be used for signalling among threads.
 * This is an important distinction as it allows threads to cooperatively work towards completing a task.
 * A mutex, on the other hand, is strictly limited to serializing access to shared state among competing threads.
 */
public class SemaphoreDemo {
}
