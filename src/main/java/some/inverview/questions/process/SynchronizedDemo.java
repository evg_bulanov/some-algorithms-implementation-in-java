package some.inverview.questions.process;

/**
 * - for static methods, the monitor will be the class object, which is distinct from the monitor of each instance of the same class.
 * - if an uncaught exception occurs in a synchronized method, the monitor is still released.
 * - synchronized blocks can be re-entered that is they are reentrant.
 */
public class SynchronizedDemo {
    public static void main( String args[] ) throws InterruptedException {
        (new NewbieSynchronization()).example();
    }

    static class NewbieSynchronization {
        Boolean flag = new Boolean(true);

        public void example() throws InterruptedException {
            Thread t1 = new Thread(new Runnable() {
                public void run() {
                    synchronized (flag) {
                        try {
                            while (flag) {
                                System.out.println("First thread about to sleep");
                                Thread.sleep(5000);
                                System.out.println("Woke up and about to invoke wait()");
                                flag.wait();
                            }
                        } catch (InterruptedException ie) {

                        }
                    }
                }
            });

            Thread t2 = new Thread(new Runnable() {
                public void run() {
//                    flag = false;
                    System.out.println("Boolean assignment done.");
                }
            });

            t1.start();
            Thread.sleep(1000);
            t2.start();
            t1.join();
            t2.join();
        }
    }
}
