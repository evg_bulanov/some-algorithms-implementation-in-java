package some.inverview.questions.process;

/**
 * User threads are high-priority threads.
 * The JVM will wait for any user thread to complete its task before terminating it.
 */
public class ThreadUserDemo {
    public void spawnThread() {
        Thread innerThread = new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 100; i++) {
                    System.out.println("I am a new thread [" + i + "]!" );
                }
            }
        });

        innerThread.start();
        System.out.println("Main thread exiting");
    }

    public static void main(String[] args) {
        new ThreadUserDemo().spawnThread();
    }
}
