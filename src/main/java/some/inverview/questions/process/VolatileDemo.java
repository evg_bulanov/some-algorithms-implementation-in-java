package some.inverview.questions.process;

/**
 * If you have a variable say a counter that is being worked on by a thread, it is possible the thread keeps a copy
 * of the counter variable in the CPU cache and manipulates it rather than writing to the main memory.
 * The JVM will decide when to update the main memory with the value of the counter, even though other threads
 * may read the value of the counter from the main memory and may end up reading a stale value.
 *
 * If a variable is declared volatile then whenever a thread writes or reads to the volatile variable,
 * the read and write always happen in the main memory. As a further guarantee, all the variables that are visible
 * to the writing thread also get written-out to the main memory alongside the volatile variable. Similarly,
 * all the variables visible to the reading thread alongside the volatile variable will have
 * the latest values visible to the reading thread.
 */
public class VolatileDemo {
    static class TaleOfTwoThreads {
        // it will never print "All done" without volatile
        /*volatile */ boolean flag = true;

        public void signalToStop() {
            flag = false;
        }
        public void run() {
            while (flag) {
                // ... Run like crazy
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TaleOfTwoThreads example = new TaleOfTwoThreads();

        // start runner thread
        Thread runner = new Thread(() -> example.run());
        runner.start();

        // wait for one second before signalling to stop
        Thread.sleep(1000);
        Thread stopper = new Thread(() -> example.signalToStop());
        stopper.start();

        // wait for two threads to complete processing
        stopper.join();
        runner.join();
        System.out.println("All Done");
    }
}
