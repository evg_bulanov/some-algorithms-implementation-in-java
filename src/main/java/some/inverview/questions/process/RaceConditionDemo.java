package some.inverview.questions.process;

import java.util.Random;

/**
 * A race condition occurs when the correctness of a computation depends on the relative timing or interleaving of multiple
 * threads by the runtime; in other words, getting the right answer relies on lucky timing.
 */
public class RaceConditionDemo {
    public static void main(String[] args) throws InterruptedException {
        RaceCondition.runTest();
    }
}

/**
 * A race condition happens when the printer thread executes a test-then-act if clause, which checks if the shared
 * variable is divisible by 5 but before the thread can print the variable out, its value is changed by the modifier thread.
 * Some of the printed values aren't divisible by 5 which verifies the existence of a race condition in the code.
 */
class RaceCondition {
    int randInt;
    Random random = new Random(System.currentTimeMillis());

    void printer() {
        int i = 1000000;
        while (i != 0) {
            if (randInt % 5 == 0) {
                if (randInt % 5 != 0)
                    System.out.println(randInt);
            }
            i--;
        }
    }

    void modifier() {
        int i = 1000000;
        while (i != 0) {
            randInt = random.nextInt(1000);
            i--;
        }
    }

    public static void runTest() throws InterruptedException {
        final RaceCondition rc = new RaceCondition();
        Thread thread1 = new Thread(rc::printer);
        Thread thread2 = new Thread(rc::modifier);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();
    }
}