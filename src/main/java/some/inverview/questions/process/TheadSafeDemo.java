package some.inverview.questions.process;

/**
 * Class is thread safe if it behaves correctly when accessed from multiple threads, irrespective of the scheduling
 * or the interleaving of the execution of those threads by the runtime environment, and with no additional synchronization
 * or other coordination on the part of the calling code.
 */
public class TheadSafeDemo {
    /**
     * All stateless objects and their corresponding classes are thread-safe.
     * ! stateless objects are thread-safe !
     */
    public static class Sum {
        int sum(int... vals) {

            int total = 0;
            for (int i = 0; i < vals.length; i++) {
                total += vals[i];
            }
            return total;
        }
    }
}
