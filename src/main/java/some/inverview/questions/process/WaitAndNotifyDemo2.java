package some.inverview.questions.process;

public class WaitAndNotifyDemo2 {
    static class WhatsWrong {
        Object object = new Object();
        Object lock = new Object();
        Boolean flag = true;

        void doSomethingImportant() throws InterruptedException {
            synchronized (object) {
                while (flag) {
                    lock.wait();
                }

                // ... Proceed to do something important
            }
        }

        void changeFlag() {
            flag = !flag;
            synchronized (object) {
                lock.notify();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new WhatsWrong().doSomethingImportant();
        // result Exception in thread "main" java.lang.IllegalMonitorStateException: current thread is not owner
        // because wait / notify is done not on monitor owner (not in synchronized by this object)
    }
}
