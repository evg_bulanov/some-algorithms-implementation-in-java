package some.inverview.questions.local_classes;

/**
 * A class that is defined in a block is called a local class.
 *
 * Usually, local classes are defined inside a method,
 * though they can also be defined inside a for loop or even an if statement.
 * Local classes do have access to members of their enclosing class.
 * They can also access local variables, but they need to be declared final.
 *
 * Anonymous classes -
 * local class except they don't have a name and should be used in place of a
 * local class when the intended use is only one time.
 *
 *
 */
class LocalClassExample {
    public static void main( String args[] ) {
        String name = "mainClass";

        // Declare our local class
        class LocalClass {
            String myName = "superFineLocalClass";
            public LocalClass(String name) {
                this.myName = name;
            }

            public void print() {
                System.out.println("My name is " + myName + " and I am enclosed by " + name);
            }
        }


        LocalClass lc1 = new LocalClass("test");
        LocalClass lc2 = new LocalClass("rest");

        lc1.print();
        lc2.print();

        // Anonymous classes
        Thread t = new Thread() {
            @Override
            public void run() {
                System.out.println("I just extended the thread class.");
            }
        };
        t.start();

    }
}