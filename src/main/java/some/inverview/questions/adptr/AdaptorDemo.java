package some.inverview.questions.adptr;
/*
|---------------|     |-----------------------|
| Client        | --> | Client interface      |
|---------------|     | + methodData(data)    |
                      |-----------------------|
                                 ^
                                 |
                      |----------------------------|
                      | Adapter                    |          |-------------------------------
                      | - adaptee: Service         | -------> | Service                      |
                      |                            |          | + serviceMethod(specialData) |
                      | + method(data)             |          |------------------------------|
                      |----------------------------|
*/
public class AdaptorDemo {
    interface XmlData {}
    interface JsonData {}

    interface IMultiRestoApp {
        void displayMenu(XmlData xmlData);
        void displayRecommendations(XmlData xmlData);
    }

    static class MultiRestoApp implements IMultiRestoApp {
        @Override
        public void displayMenu(XmlData xmlData) {
        }

        @Override
        public void displayRecommendations(XmlData xmlData) {
        }
    }

    static class FancyService {
        public void displayMenu(JsonData xmlData) {
            // use json to display menu
        }

        public void displayRecommendations(JsonData xmlData) {
            // use json to load recommendation
        }
    }

    static class FancyServiceAdapter implements IMultiRestoApp {
        private FancyService fancyService = new FancyService();
        @Override
        public void displayMenu(XmlData xmlData) {
            fancyService.displayMenu(convertJsonData(xmlData));
        }

        @Override
        public void displayRecommendations(XmlData xmlData) {
            fancyService.displayRecommendations(convertJsonData(xmlData));
        }

        JsonData convertJsonData(XmlData xmlData) {
            return new JsonData() {};
        }
    }

    public static void main(String[] args) {
        IMultiRestoApp multiRestoApp = new MultiRestoApp();
        multiRestoApp.displayMenu(new XmlData() {});

        FancyServiceAdapter adapter = new FancyServiceAdapter();
        adapter.displayMenu(new XmlData() {});
    }
}
