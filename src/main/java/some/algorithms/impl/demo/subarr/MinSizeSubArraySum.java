package some.algorithms.impl.demo.subarr;

public class MinSizeSubArraySum {
    /*
    * Given an array of positive numbers and a positive number ‘S’,
    * find the length of the smallest contiguous subarray whose sum is greater than or equal to ‘S’.
    * Return 0, if no such subarray exists.
    *
    * {2, 1, 5, 2, 3, 2}
    *  -
    *  ----
    *  -------
    */
    public static int findMinSubArray(int S, int[] arr) {
        int windowStart = 0;
        int minLength = Integer.MAX_VALUE;
        int windowSum = 0;
        for (int windowsEnd = 0; windowsEnd < arr.length; windowsEnd++) {
            windowSum += arr[windowsEnd];

            while (windowSum >= S) {
                minLength = Math.min(minLength, windowsEnd - windowStart + 1);
                windowSum -= arr[windowStart];
                windowStart++;
            }
        }

        return minLength;
    }
}
