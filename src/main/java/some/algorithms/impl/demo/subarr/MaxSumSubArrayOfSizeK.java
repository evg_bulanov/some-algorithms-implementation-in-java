package some.algorithms.impl.demo.subarr;

import java.util.Arrays;

public class MaxSumSubArrayOfSizeK {

    public static int findMaxSumSubArray(int k, int[] arr) {
        int maxSum = Integer.MIN_VALUE;
        int[] maxSubArr = new int[k];
        for (int i = 0; i < arr.length; i++) {
            int maxSumOfSubArr = 0;
            int[] subArr = new int[k];
            for (int j = 0; j < k && i + j < arr.length ; j++) {
                subArr[j] = arr[ i+ j];
                maxSumOfSubArr += arr[i + j];
            }
            if (maxSum < maxSumOfSubArr) {
                maxSubArr = subArr;
                maxSum = maxSumOfSubArr;
            }
        }

        Arrays.stream(maxSubArr).forEach(System.out::println);
        return maxSum;
    }

    public static int findMaxSumSubArray2(int k, int[] arr) {
        int maxSum = 0, windowSum;
        for (int i = 0; i <= arr.length - k; i++) {
            windowSum = 0;
            for (int j = i; j < i + k; j++) {
                windowSum += arr[j];
            }
            maxSum = Math.max(maxSum, windowSum);
        }

        return maxSum;
    }

    public static int findMaxSumSubArray3(int k, int[] arr) {
        int maxSum = 0;
        int windowSum = 0;
        int windowStart = 0;
        for (int windowEnd = 0; windowEnd < arr.length; windowEnd++) {
            windowSum += arr[windowEnd];
            if (windowEnd >= k - 1) {
                maxSum = Math.max(maxSum, windowSum);
                windowSum -= arr[windowStart];
                windowStart++;
            }
        }
        return maxSum;
    }


    public static void main(String[] args) {
        int[] arr = { 2, 1, 5, 1, 3, 2};
        int k = 3;

        int maxSum = findMaxSumSubArray(k, arr);
        System.out.println("maxSum = " + maxSum);

        maxSum = findMaxSumSubArray2(k, arr);
        System.out.println("maxSum = " + maxSum);

        maxSum = findMaxSumSubArray3(k, arr);
        System.out.println("maxSum = " + maxSum);
    }
}
