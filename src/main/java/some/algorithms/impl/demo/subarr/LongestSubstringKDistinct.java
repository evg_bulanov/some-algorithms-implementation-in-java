package some.algorithms.impl.demo.subarr;

import java.util.HashMap;
import java.util.Map;

public class LongestSubstringKDistinct {
    /**
     * Given a string, find the length of the longest substring in it with no more than K distinct characters.
     *
     * @param str
     * @param k
     * @return
     */
    public static int findLength(String str, int k) {
        int windowStart = 0;
        int maxLength = Integer.MIN_VALUE;
        Map<Character, Integer> map = new HashMap<>();

        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            char chr = str.charAt(windowEnd);

            int counter = map.getOrDefault(chr, 0) + 1;
            map.put(chr, counter);

            if (map.size() > k) {
                char startChar = str.charAt(windowStart);
                Integer strCnt = map.get(startChar) - 1;
                if (strCnt == 0) {
                    map.remove(startChar);
                } else {
                    map.put(startChar, strCnt);
                }
                maxLength = Math.max(maxLength, windowEnd - windowStart);
                windowStart++;
            }

        }
        return maxLength;
    }

    public static int findLength2(String str, int k) {
        if (str == null || str.length() == 0 || str.length() < k)
            throw new IllegalArgumentException();

        int windowStart = 0, maxLength = 0;
        Map<Character, Integer> charFrequencyMap = new HashMap<>();
        // in the following loop we'll try to extend the range [windowStart, windowEnd]
        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            char rightChar = str.charAt(windowEnd);
            charFrequencyMap.put(rightChar, charFrequencyMap.getOrDefault(rightChar, 0) + 1);
            // shrink the sliding window, until we are left with 'k' distinct characters in the frequency map
            while (charFrequencyMap.size() > k) {
                char leftChar = str.charAt(windowStart);
                charFrequencyMap.put(leftChar, charFrequencyMap.get(leftChar) - 1);
                if (charFrequencyMap.get(leftChar) == 0) {
                    charFrequencyMap.remove(leftChar);
                }
                windowStart++; // shrink the window
            }
            maxLength = Math.max(maxLength, windowEnd - windowStart + 1); // remember the maximum length so far
        }

        return maxLength;
    }
}
