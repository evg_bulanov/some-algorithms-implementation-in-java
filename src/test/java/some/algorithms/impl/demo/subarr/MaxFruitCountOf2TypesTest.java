package some.algorithms.impl.demo.subarr;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxFruitCountOf2TypesTest {
    @Test
    void test1() {
        // given:
        char[] arr = {'A', 'B', 'C', 'A', 'C'};

        // when:
        int length = MaxFruitCountOf2Types.findLength(arr);

        // then:
        assertEquals(3, length);
    }
}