package some.algorithms.impl.demo.subarr;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestSubstringKDistinctTest {
    /**
     * Input: String="araaci", K=2
     * Output: 4
     * Explanation: The longest substring with no more than '2' distinct characters is "araa".
     */
    @Test
    void test1() {
        // given:
        String str = "araaci";
        int k = 2;

        // when:
        int actual = LongestSubstringKDistinct.findLength(str, k);

        // then:
        assertEquals(4, actual);
    }

    /**
     * Input: String="araaci", K=1
     * Output: 2
     * Explanation: The longest substring with no more than '1' distinct characters is "aa".
     */
    @Test
    void test2() {
        // given:
        String str = "araaci";
        int k = 1;

        // when:
        int actual = LongestSubstringKDistinct.findLength(str, k);

        // then:
        assertEquals(2, actual);
    }

    /**
     * Input: String="cbbebi", K=3
     * Output: 5
     * Explanation: The longest substrings with no more than '3' distinct characters are "cbbeb" & "bbebi".
     */
    @Test
    void test3() {
        // given:
        String str = "cbbebi";
        int k = 3;

        // when:
        int actual = LongestSubstringKDistinct.findLength(str, k);

        // then:
        assertEquals(5, actual);
    }
}