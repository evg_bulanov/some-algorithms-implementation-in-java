package some.algorithms.impl.demo.subarr;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinSizeSubArraySumTest {

    @Test
    void test1() {
        // given:
        int arr[] = {2, 1, 5, 2, 3, 2};
        int s = 7;

        // when:
        int actual = MinSizeSubArraySum.findMinSubArray(s, arr);
        System.out.println(actual);

        // then:
        assertEquals(2, actual);
    }

    @Test
    void test2() {
        // given:
        int arr[] = {2, 1, 5, 2, 8};
        int s = 7;

        // when:
        int actual = MinSizeSubArraySum.findMinSubArray(s, arr);
        System.out.println(actual);

        // then:
        assertEquals(1, actual);
    }

    @Test
    void test3() {
        // given:
        int arr[] = {3, 4, 1, 1, 6};
        int s = 8;

        // when:
        int actual = MinSizeSubArraySum.findMinSubArray(s, arr);
        System.out.println(actual);

        // then:
        assertEquals(3, actual);
    }
}